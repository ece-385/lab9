/************************************************************************
Lab 9 Nios Software

Dong Kai Wang, Fall 2017
Christine Chen, Fall 2013

For use with ECE 385 Experiment 9
University of Illinois ECE Department
************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "aes.h"

// Pointer to base address of AES module, make sure it matches Qsys
volatile unsigned int * AES_PTR = (unsigned int *) 0x00000100;

// Execution mode: 0 for testing, 1 for benchmarking
int run_mode = 0;

/** charToHex
 *  Convert a single character to the 4-bit value it represents.
 *
 *  Input: a character c (e.g. 'A')
 *  Output: converted 4-bit value (e.g. 0xA)
 */
char charToHex(char c)
{
	char hex = c;

	if (hex >= '0' && hex <= '9')
		hex -= '0';
	else if (hex >= 'A' && hex <= 'F')
	{
		hex -= 'A';
		hex += 10;
	}
	else if (hex >= 'a' && hex <= 'f')
	{
		hex -= 'a';
		hex += 10;
	}
	return hex;
}

/** charsToHex
 *  Convert two characters to byte value it represents.
 *  Inputs must be 0-9, A-F, or a-f.
 *
 *  Input: two characters c1 and c2 (e.g. 'A' and '7')
 *  Output: converted byte value (e.g. 0xA7)
 */
char charsToHex(char c1, char c2)
{
	char hex1 = charToHex(c1);
	char hex2 = charToHex(c2);
	return (hex1 << 4) + hex2;
}

/*CODE BY DAVID ANTONOWICZ BEGINS HERE*/

/**RotWord: Called by KeyExpansion function to rotate each of the cells of the current key*/
Performs
unsigned int RotWord(unsigned int temp){
	unsigned int result = 0;

	unsigned int cell0 = (temp & 0x00FF0000) << 8;				//rotate word by masking other bits and shifting, then concatenating
  unsigned int cell1 = (temp & 0x0000FF00) << 8;
	unsigned int cell2 = (temp & 0x000000FF) << 8;
	unsigned int cell3 = (temp & 0xFF000000) >> 24;

	result = cell0 + cell1 + cell2 + cell3;
	return result;
}

/**Subword: Called by KeyExpansion Function to substitute each of the cells of the previously rotated word using the aes_sbox*/
unsigned int SubWord(unsigned int temp){
	unsigned int result = 0;

//substitute cells by masking bits and replacing with  corresponding sbox value, then concatenating
	unsigned int cell0 = aes_sbox[(temp & 0xFF000000) >> 24] << 24;
	unsigned int cell1 = aes_sbox[(temp & 0x00FF0000) >> 16] << 16;
	unsigned int cell2 = aes_sbox[(temp & 0x0000FF00) >> 8] << 8;
	unsigned int cell3 = aes_sbox[(temp & 0x000000FF)];

	result = cell0 + cell1 + cell2 + cell3;
	return result;
}
/** KeyExpansion
 * Perform a key expansion on the original key text
 *
 * Input: key_ascii - Pointer to 32x 8-bit char array that contains the input key in ASCII format
 * Output: 		key	- Pointer to 4x 32-bit int array that contains the input key
 */
void KeyExpansion(unsigned char * hex_key, unsigned int * key_schedule) {
	unsigned int temp;
	for(int i = 0; i < 4; i++){ //loop for setting initial round key
		key_schedule[i] = (hex_key[4*i]<<24) + (hex_key[(4*i)+1]<<16) + (hex_key[(4*i)+2]<<8) + (hex_key[(4*i)+3]);
	}
	for(int i = 4; i < 44; i++){
		temp = key_schedule[i-1];
		if (i % 4 == 0){
			temp = SubWord(RotWord(temp)) ^ Rcon[i/4];
		}
		key_schedule[i] = (key_schedule [i-4]) ^ temp;
	}
}
 
 /**AddRoundKey: Adds the round key to the current message state*/
void AddRoundKey(unsigned char *state_cell, unsigned int *round_key){
		for(int i=0; i < 4; i++){
		state_cell[i*4] = state_cell[i*4] ^ (round_key[i] >> 24); //round_key requires conversion to 8-bit char data type
		state_cell[i*4+1] = state_cell[i*4+1] ^ (round_key[i] >> 16);
		state_cell[i*4+2] = state_cell[i*4+2] ^ (round_key[i] >> 8);
		state_cell[i*4+3] = state_cell[i*4+3] ^ (round_key[i]);
		}

	}

/**SubBytes: Substitute each of the cells of the current message state using the AES LUT*/
void SubBytes(unsigned char *state_cell){ //THIS WORKS
	for(int i = 0; i < 16; i++){
		state_cell[i] = aes_sbox[state_cell[i]];
	}
}

/*Left circultar shift each of the rows of the current message by an increasing number of cells based on row*/
void ShiftRows(unsigned char * state_cell){ //THIS WORKS
		unsigned char temp[16];
		//left shift 0 cells
		temp[0] = state_cell[0];
		temp[4] = state_cell[4];
		temp[8] = state_cell[8];
		temp[12] = state_cell[12];
		//left shift 1 cell
		temp[1] = state_cell[5];
		temp[5] = state_cell[9];
		temp[9] = state_cell[13];
		temp[13] = state_cell[1];
		//left shift 2 cells
		temp[2] = state_cell[10];
		temp[6] = state_cell[14];
		temp[10] = state_cell[2];
		temp[14] = state_cell[6];
		//left shift 3 cells
		temp[3] = state_cell[15];
		temp[7] = state_cell[3];
		temp[11] = state_cell[7];
		temp[15] = state_cell[11];

		for(int i = 0; i < 16; i++){
			state_cell[i] = temp[i];
		}
}

/*Subroutine called by mix columns to calculate new cell value in Galois Field*/
unsigned char xtime(unsigned char cell){
	int flag = 0;
	if ((cell>>7) & 1){
		flag = 1;
	}
	cell = cell << 1;
	if(flag){
		cell = cell ^ 0x1b;
	}
	return cell;
}

/*MixColumns: Calculates new values for each of the cells in the current message state using Galois Field operations*/
void MixColumns(unsigned char * state_cell){
	unsigned char temp[16];

	//column 0 calculation
	temp[0] = xtime(state_cell[0]) ^ (xtime(state_cell[1]) ^ state_cell[1]) ^ state_cell[2] ^ state_cell[3];
	temp[1] = state_cell[0] ^ xtime(state_cell[1]) ^ (xtime(state_cell[2]) ^ state_cell[2]) ^ state_cell[3];
	temp[2] = state_cell[0] ^ state_cell[1] ^ xtime(state_cell[2]) ^ (xtime(state_cell[3]) ^ state_cell[3]);
	temp[3] = (xtime(state_cell[0]) ^ state_cell[0]) ^ state_cell[1] ^ state_cell[2] ^ xtime(state_cell[3]);
	//column 1 calculation
	temp[4] = xtime(state_cell[4]) ^ (xtime(state_cell[5]) ^ state_cell[5]) ^ state_cell[6] ^ state_cell[7];
	temp[5] = state_cell[4] ^ xtime(state_cell[5]) ^ (xtime(state_cell[6]) ^ state_cell[6]) ^ state_cell[7];
	temp[6] = state_cell[4] ^ state_cell[5] ^ xtime(state_cell[6]) ^ (xtime(state_cell[7]) ^ state_cell[7]);
	temp[7] = (xtime(state_cell[4]) ^ state_cell[4]) ^ state_cell[5] ^ state_cell[6] ^ xtime(state_cell[7]);
	//column 3 calculation
	temp[8] = xtime(state_cell[8]) ^ (xtime(state_cell[9]) ^ state_cell[9]) ^ state_cell[10] ^ state_cell[11];
	temp[9] = state_cell[8] ^ xtime(state_cell[9]) ^ (xtime(state_cell[10]) ^ state_cell[10]) ^ state_cell[11];
	temp[10] = state_cell[8] ^ state_cell[9] ^ xtime(state_cell[10]) ^ (xtime(state_cell[11]) ^ state_cell[11]);
	temp[11] = (xtime(state_cell[8]) ^ state_cell[8]) ^ state_cell[9] ^ state_cell[10] ^ xtime(state_cell[11]);
	//column 3 calculation
	temp[12] = xtime(state_cell[12]) ^ (xtime(state_cell[13]) ^ state_cell[13]) ^ state_cell[14] ^ state_cell[15];
	temp[13] = state_cell[12] ^ xtime(state_cell[13]) ^ (xtime(state_cell[14]) ^ state_cell[14]) ^ state_cell[15];
	temp[14] = state_cell[12] ^ state_cell[13] ^ xtime(state_cell[14]) ^ (xtime(state_cell[15]) ^ state_cell[15]);
	temp[15] = (xtime(state_cell[12]) ^ state_cell[12]) ^ state_cell[13] ^ state_cell[14] ^ xtime(state_cell[15]);

	for(int i = 0; i < 16; i++){
		state_cell[i] = temp[i];
	}
}



/** encrypt
 *  Perform AES encryption in software.
 *
 *  Input: msg_ascii - Pointer to 32x 8-bit char array that contains the input message in ASCII format
 *         key_ascii - Pointer to 32x 8-bit char array that contains the input key in ASCII format
 *  Output:  msg_enc - Pointer to 4x 32-bit int array that contains the encrypted message
 *               key - Pointer to 4x 32-bit int array that contains the input key
 */
void encrypt(unsigned char * msg_ascii, unsigned char * key_ascii, unsigned int * msg_enc, unsigned int * key)
{
	// Implement this function

	unsigned char state_cell[16];
	unsigned char hex_key[16];
	unsigned int key_schedule[44];
	unsigned int round_key[4];

	for (int i = 0; i < 16; i++) {
		state_cell[i] = charsToHex(msg_ascii[2*i],msg_ascii[2*i+1]);
		hex_key[i] = charsToHex(key_ascii[2*i], key_ascii[2*i+1]);
	}

	KeyExpansion(hex_key, key_schedule);

	for(int i = 0; i < 4; i++){//set initial round_key
		round_key[i] = key_schedule[i];
  }

	AddRoundKey(state_cell, round_key);

	for(int round = 1; round < 10; round++){
		SubBytes(state_cell);
		ShiftRows(state_cell);
		MixColumns(state_cell);
		for(int i = 0; i < 4; i++){
			round_key[i] = key_schedule[4*round + i];			//update round_key from key schedule for AddRoundKey
		}
		AddRoundKey(state_cell, round_key);
	}

	for(int i = 0; i < 4; i++){
		round_key[i] = key_schedule[40+i];			//update final round_key from key schedule for last AddRoundKey
	}
	SubBytes(state_cell);
	ShiftRows(state_cell);
	AddRoundKey(state_cell, round_key); //final AddRoundKey

	for (int j = 0; j < 4; j++) {
		msg_enc[j] = (state_cell[4*j]<<24) + (state_cell[4*j+1]<<16) + (state_cell[4*j+2]<<8) + (state_cell[4*j+3]);
		key[j] = (hex_key[4*j]<<24) + (hex_key[4*j+1]<<16) + (hex_key[4*j+2]<<8) + (hex_key[4*j+3]);
	}

	AES_PTR[0]=key[0];
	AES_PTR[1]=key[1];
	AES_PTR[2]=key[2];
	AES_PTR[3]=key[3];

	AES_PTR[4]=msg_enc[0];
	AES_PTR[5]=msg_enc[1];
	AES_PTR[6]=msg_enc[2];
	AES_PTR[7]=msg_enc[3];
}


/** decrypt
 *  Perform AES decryption in hardware.
 *
 *  Input:  msg_enc - Pointer to 4x 32-bit int array that contains the encrypted message
 *              key - Pointer to 4x 32-bit int array that contains the input key
 *  Output: msg_dec - Pointer to 4x 32-bit int array that contains the decrypted message
 */
void decrypt(unsigned int * msg_enc, unsigned int * msg_dec, unsigned int * key)
{
	AES_PTR[14] = 1;
	while(!AES_PTR[15]){}
	msg_dec[0] = AES_PTR[8];
	msg_dec[1] = AES_PTR[9];
	msg_dec[2] = AES_PTR[10];
	msg_dec[3] = AES_PTR[11];
	AES_PTR[14] = 0;
}

/*CODE BY DAVID ANTONOWICZ ENDS HERE*/

/** main
 *  Allows the user to enter the message, key, and select execution mode
 *
 */
int main()
{
	// Input Message and Key as 32x 8-bit ASCII Characters ([33] is for NULL terminator)
	unsigned char msg_ascii[33];
	unsigned char key_ascii[33];
	// Key, Encrypted Message, and Decrypted Message in 4x 32-bit Format to facilitate Read/Write to Hardware
	unsigned int key[4];
	unsigned int msg_enc[4];
	unsigned int msg_dec[4];

	printf("Select execution mode: 0 for testing, 1 for benchmarking: ");
	scanf("%d", &run_mode);

	if (run_mode == 0) {
		// Continuously Perform Encryption and Decryption
		while (1) {
			int i = 0;
			printf("\nEnter Message:\n");
			scanf("%s", msg_ascii);
			printf("\n");
			printf("\nEnter Key:\n");
			scanf("%s", key_ascii);
			printf("\n");

			encrypt(msg_ascii, key_ascii, msg_enc, key);
			
			printf("\nEncrypted message is: \n");
			for(i = 0; i < 4; i++){
				printf("%08x", msg_enc[i]);
			}
			printf("\n");
			decrypt(msg_enc, msg_dec, key);
			printf("\nDecrypted message is: \n");
			for(i = 0; i < 4; i++){
				printf("%08x", msg_dec[i]);
			}
			printf("\n");
		}
	}
	else {
		// Run the Benchmark
		int i = 0;
		int size_KB = 2;
		// Choose a random Plaintext and Key
		for (i = 0; i < 32; i++) {
			msg_ascii[i] = 'a';
			key_ascii[i] = 'b';
		}
		// Run Encryption
		clock_t begin = clock();
		for (i = 0; i < size_KB * 64; i++)
			encrypt(msg_ascii, key_ascii, msg_enc, key);
		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		double speed = size_KB / time_spent;
		printf("Software Encryption Speed: %f KB/s \n", speed);
		// Run Decryption
		begin = clock();
		for (i = 0; i < size_KB * 64; i++)
			decrypt(msg_enc, msg_dec, key);
		end = clock();
		time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		speed = size_KB / time_spent;
		printf("Hardware Encryption Speed: %f KB/s \n", speed);
	}
	return 0;
}
